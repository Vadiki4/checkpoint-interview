# CheckPoint interview task

## Intallation steps

- npm install
- npm run start-db - starts Rest like server from fake json db. (keep it running) [requires nodejs > 10]
- Start the app with npm start

**Note: Tested on latest versions of Firefox and Chrome**

## React App

- 1 screen [Dashboard] (with option for more)
- Redux & Saga for global state managment and side effects
- Reselect for memoization of calculations
- Styled Components
- Table with virtual list using react-window (although it still works not as I would expect)
- 1 tab has big amount of data to test the table
- Layouts for specific parts of a page to allow reusability and customization
- Persistant redux store for session - allow to show already recieved data before new
- .env file with outer configuration
- create and download CSV files
- debounced category filter
- Error Boundary

## Fake DB server

- starts REST like API endpoint from a fake json db.
