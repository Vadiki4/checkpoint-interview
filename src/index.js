import React from 'react';
import ReactDOM from 'react-dom';
import Router from './Router';
import { Provider } from 'react-redux';
import store from './createStore';
import 'ress';
import 'antd/dist/antd.css';
import './App.scss';
import ErrorBoundary from './Components/Common/ErrorBoundary'; // catch unexpected errors

ReactDOM.render(
	<Provider store={store}>
		<ErrorBoundary>
			<Router />
		</ErrorBoundary>
	</Provider>,
	document.getElementById('root')
);
