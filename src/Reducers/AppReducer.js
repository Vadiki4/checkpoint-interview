import produce from 'immer';
import { SET_LOADING } from '../Sagas/actions';

const initialState = {
	loading: false,
};

export default (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case SET_LOADING: {
				draft.loading = action.payload;
				return draft;
			}
			default:
				return draft;
		}
	});
