import { combineReducers } from 'redux';
import App from './AppReducer';
import Mails from './MailsReducer';

const reducers = combineReducers({
	app: App,
	mails: Mails,
});

export default reducers;
