import produce from 'immer';
import { SAVE_MAIL_LIST, SET_MAIL_TYPE } from '../Sagas/actions';

const initialState = {
	mailList: {},
	mailTypes: ['Release requests', 'All Quarantine mails'],
	selectedMailType: 'Release requests',
	categories: [],
};

export default (state = initialState, action) =>
	produce(state, draft => {
		switch (action.type) {
			case SAVE_MAIL_LIST: {
				const { mailList, selectedMailType } = action.payload;
				if (mailList && selectedMailType) {
					if (!draft.mailList[selectedMailType]) draft.mailList[selectedMailType] = [];
					draft.mailList[selectedMailType] = mailList;
				}

				// update categories to ensure any new category is inside
				draft.categories = [];
				for (const item of mailList) {
					if (!item.category) continue;
					if (draft.categories.indexOf(item.category) < 0) {
						draft.categories.push(item.category);
					}
				}

				return draft;
			}
			case SET_MAIL_TYPE: {
				const { mailType } = action.payload;
				if (mailType) {
					draft.selectedMailType = mailType;
				}
				return draft;
			}
			default:
				return draft;
		}
	});
