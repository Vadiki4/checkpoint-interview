import { takeLatest } from 'redux-saga/effects';
import { getMailList, deleteRowsFromDB } from './mailsSagas';
import { GET_MAIL_LIST, DELETE_ROWS_FROM_DB } from '../Sagas/actions';

// register saga listeners
export function* mailsWatcher() {
	yield takeLatest(GET_MAIL_LIST, getMailList);
	yield takeLatest(DELETE_ROWS_FROM_DB, deleteRowsFromDB);
}
