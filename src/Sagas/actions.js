export const GET_MAIL_LIST = 'GET_MAIL_LIST';
export const SAVE_MAIL_LIST = 'SAVE_MAIL_LIST';
export const SET_LOADING = 'SET_LOADING';
export const SET_MAIL_TYPE = 'SET_MAIL_TYPE';
export const DELETE_ROWS_FROM_DB = 'DELETE_ROWS_FROM_DB';
