import { call, put } from 'redux-saga/effects';
import Api from '../Utilities/Api';
import { SAVE_MAIL_LIST, SET_LOADING } from './actions';
import { message } from 'antd';

export function* getMailList(action) {
	try {
		yield put({ type: SET_LOADING, payload: true });
		const { selectedMailType } = action.payload;
		const response = yield call(() => Api.mails().getMailList({ selectedMailType }));
		if (response.status === 200) {
			yield put({
				type: SAVE_MAIL_LIST,
				payload: { mailList: response.data, selectedMailType },
			});
			yield put({ type: SET_LOADING, payload: false });
		} else {
			message.warn('Bad login');
			yield put({ type: SET_LOADING, payload: false });
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		yield put({ type: SET_LOADING, payload: false });
		console.error(err);
	}
}

export function* deleteRowsFromDB(action) {
	try {
		yield put({ type: SET_LOADING, payload: true });
		const { selectedMailRows, selectedMailType } = action.payload;
		yield call(() => Api.mails().deleteMails({ selectedMailRows }));
		const response = yield call(() => Api.mails().getMailList({ selectedMailType }));
		if (response.status === 200) {
			yield put({
				type: SAVE_MAIL_LIST,
				payload: { mailList: response.data, selectedMailType },
			});
			yield put({ type: SET_LOADING, payload: false });
		} else {
			message.warn('Bad login');
			yield put({ type: SET_LOADING, payload: false });
			// TODO : dispatch error to UI
		}
	} catch (err) {
		message.warn('Network Error');
		yield put({ type: SET_LOADING, payload: false });
		console.error(err);
	}
}
