// saving store to session storage
// for serving already loaded data before new arrives

export const loadState = () => {
	try {
		const serializedState = sessionStorage.getItem('state');
		if (serializedState === null) {
			return undefined;
		}
		return JSON.parse(serializedState);
	} catch (error) {
		console.error(error);
	}
};

export const setState = state => {
	try {
		const serializedState = JSON.stringify(state);
		sessionStorage.setItem('state', serializedState);
	} catch (error) {
		console.error(error);
	}
};
