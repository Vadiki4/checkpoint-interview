import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Dashboard from './Screens/Dashboard';

// In our case routing isnt necessary but added for future app development
function App() {
	return (
		<div className='App'>
			<Router>
				<Switch>
					<Route path='/' component={Dashboard} />
					<Redirect to='/' />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
