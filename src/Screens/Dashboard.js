import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TablePageLayout from '../Components/Layouts/TablePageLayout';
import { selectedMailTypeList, getSelectedMailType, getMailTypes, getMailCategories, getAppLoadingState } from '../Selectors';
import { GET_MAIL_LIST, SET_MAIL_TYPE, DELETE_ROWS_FROM_DB } from '../Sagas/actions';

function Home() {
	const selectedMailType = useSelector(getSelectedMailType);
	const mailTypes = useSelector(getMailTypes);
	const dispatch = useDispatch();
	const selectedMailList = useSelector(selectedMailTypeList);
	const mailCategories = useSelector(getMailCategories);
	const appLoadingState = useSelector(getAppLoadingState);

	// loading data when mail type (tab) changed
	// will reload data on each change for accurate results and updates from db
	// can be loaded only once with existance check
	useEffect(() => {
		dispatch({ type: GET_MAIL_LIST, payload: { selectedMailType } });
	}, [selectedMailType]);

	function handleMailTypeSelect(mailType) {
		dispatch({ type: SET_MAIL_TYPE, payload: { mailType } });
	}

	function deleteRowsFromDB(selectedMailRows) {
		dispatch({ type: DELETE_ROWS_FROM_DB, payload: selectedMailRows });
	}

	// Using Layouts for easy reuse of content in different screens and custom screen customization
	return (
		<TablePageLayout
			mailList={selectedMailList}
			handleMailTypeSelect={handleMailTypeSelect}
			deleteRowsFromDB={deleteRowsFromDB}
			appLoadingState={appLoadingState}
			selectedMailType={selectedMailType}
			mailTypes={mailTypes}
			mailCategories={mailCategories}
		/>
	);
}

export default Home;
