import axios from 'axios';
const baseUrl = process.env.REACT_APP_API_ENDPOINT;

export default {
	mails() {
		const url = baseUrl + '/mails';
		return {
			getMailList: data => {
				const { selectedMailType } = data;
				return axios.get(`${url}?mail_type=${selectedMailType}`);
			},
			deleteMails: data => {
				const { selectedMailRows } = data;
				return axios.delete(`${url}/${selectedMailRows[0].id}`);
			},
		};
	},
	// other() {
	// 	const url = baseUrl + '/other';
	// 	return {};
	// },
};
