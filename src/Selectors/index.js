import { createSelector } from 'reselect';
import get from 'lodash/get';

// GETTERS
export const getAppLoadingState = state => state.app.loading;
export const getMailsList = state => state.mails.mailList;
export const getSelectedMailType = state => state.mails.selectedMailType;
export const getMailTypes = state => state.mails.mailTypes;
export const getMailCategories = state => state.mails.categories;

// SELECTORS - for memoization of calculations
export const selectedMailTypeList = createSelector([getMailsList, getSelectedMailType], (mailList, mailType) => get(mailList, [mailType], []));
