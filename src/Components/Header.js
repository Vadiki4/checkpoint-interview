import React from 'react';
import { HeaderElement } from '../Components/Styled/Header';
import { FilterOutlined, CloseOutlined, DownloadOutlined, LoadingOutlined } from '@ant-design/icons';
import Button from '../Components/Common/FormElements/Button';
import Search from '../Components/Common/FormElements/Search';

function Header({ toggleFilterBar, itemsCount, appLoadingState, downloadData, handleDelete }) {
	return (
		<HeaderElement>
			<LoadingOutlined className={appLoadingState ? 'loading' : 'loading hidden'} />
			<div className='tool-bar'>
				<Button title='Release' icon={<FilterOutlined />} />
				<Button title='Reject Request' icon={<CloseOutlined />} onClick={handleDelete} />
				<Button title='Download EML' icon={<DownloadOutlined />} onClick={downloadData} />
				<Search placeholder='Search' />
				<Button icon={<FilterOutlined />} btnClass='filter' onClick={toggleFilterBar} />
			</div>
			<div className='counter'>
				<p>{itemsCount || 0} items</p>
			</div>
		</HeaderElement>
	);
}

export default Header;
