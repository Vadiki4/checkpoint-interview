// layouts used to contain specific details and allow itself reusability and makes its container to be more customizable

import React, { useState, useEffect } from 'react';
import { TablePageLayoutElement } from '../../Components/Styled/Pages';
import { message } from 'antd';
import Header from '../Header';
import SideBar from '../SideBar';
import FilterBar from '../FilterBar';
import TableHolder from '../TableHolder';
import Footer from '../FooterDetails';
import useDebounce from '../../Hooks/useDebounce';
import { ExportToCsv } from 'export-to-csv';

function TablePageLayout({ mailList, selectedMailType, mailTypes, mailCategories, handleMailTypeSelect, appLoadingState, deleteRowsFromDB }) {
	// could be refactored to useReducer
	const [showFilterBar, setShowFilterBar] = useState(false);
	const [selectedMailDetails, setSelectedMailDetails] = useState({});
	const [showFooterDetails, setShowFooterDetails] = useState(false);
	const [filteredMailList, setFilteredMailList] = useState([]);
	const [selectedMailCategory, setselectedMailCategory] = useState([]);
	const [selectedMailRows, setSelectedMailRows] = useState([]);
	const [selectedTableRows, setSelectedTableRows] = useState([]);
	const [selectedMailTime, setselectedMailTime] = useState([]);
	const debouncedMailCategoryChange = useDebounce(selectedMailCategory, 600);

	useEffect(() => {
		const filtered = mailList.filter(item => selectedMailCategory.indexOf(item.category) >= 0);
		setFilteredMailList(filtered);
	}, [debouncedMailCategoryChange]);

	function handleMailTypeSelectLocal(tab) {
		handleMailTypeSelect(tab);
		setShowFilterBar(false);
		setShowFooterDetails(false);
		setFilteredMailList([]);
		setselectedMailTime([]);
		setselectedMailCategory([]);
		setSelectedMailDetails({});
		setSelectedTableRows([]);
	}

	function handleDownloadData() {
		const readyList = filteredMailList.length ? filteredMailList : mailList;
		if (readyList.length) {
			const csvExporter = new ExportToCsv();
			csvExporter.generateCsv(readyList);
		} else {
			message.warn('No data');
		}
	}

	function toggleFilterBar() {
		window.requestAnimationFrame(() => setShowFilterBar(!showFilterBar));
	}

	function handleMailCategoryChange(checkedValues) {
		setselectedMailCategory(checkedValues);
	}

	function handleTimeChange(value) {
		setselectedMailTime(value);
	}

	function handleTableRowClick(record) {
		window.requestAnimationFrame(() => setShowFooterDetails(true));
		setSelectedMailDetails(record);
	}

	function toggleFooter() {
		window.requestAnimationFrame(() => setShowFooterDetails(!showFooterDetails));
	}

	function handleDelete() {
		if (selectedMailRows.length === 1) {
			deleteRowsFromDB({ selectedMailRows, selectedMailType });
			setSelectedMailRows([]);
			setSelectedTableRows([]);
		} else if (selectedMailRows.length) {
			message.warn('Only one row allowed to be removed, due to json-server library');
		} else {
			message.warn('No selected rows');
		}
	}

	function handleRowSelect(selectedRowKeys, selectedRows) {
		setSelectedMailRows(selectedRows);
		setSelectedTableRows(selectedRowKeys);
	}

	return (
		<TablePageLayoutElement height={window.innerHeight}>
			<SideBar mailTypes={mailTypes} selectedMailType={selectedMailType} handleMailTypeSelect={handleMailTypeSelectLocal} />
			<div className='layout-body'>
				<Header
					appLoadingState={appLoadingState}
					toggleFilterBar={toggleFilterBar}
					downloadData={handleDownloadData}
					handleDelete={handleDelete}
					itemsCount={filteredMailList.length ? filteredMailList.length : mailList.length}
				/>
				<div className='content'>
					<div className='table-content'>
						<TableHolder
							height={showFooterDetails ? '50%' : '100%'}
							handleRowSelect={handleRowSelect}
							selectedTableRows={selectedTableRows}
							mailList={filteredMailList.length ? filteredMailList : mailList}
							handleRowClick={handleTableRowClick}
						/>
						<Footer
							show={showFooterDetails}
							selectedMailType={selectedMailType}
							toggleFooter={toggleFooter}
							selectedMailDetails={selectedMailDetails}
						/>
					</div>
					<FilterBar
						show={showFilterBar}
						mailCategories={mailCategories}
						handleMailCategoryChange={handleMailCategoryChange}
						selectedMailCategory={selectedMailCategory}
						handleTimeChange={handleTimeChange}
						selectedMailTime={selectedMailTime}
					/>
				</div>
			</div>
		</TablePageLayoutElement>
	);
}

export default TablePageLayout;
