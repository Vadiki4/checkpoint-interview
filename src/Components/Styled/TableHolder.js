import styled from 'styled-components';

export const TableHolderElement = styled.div`
	width: 100%;
	overflow: auto;
	height: ${props => props.height};
	transition: height ease-out 600ms;
	margin-bottom: 20px;

	.ant-table-wrapper {
		overflow: hidden;

		.ant-table tbody tr {
			cursor: pointer;
		}
	}
`;
