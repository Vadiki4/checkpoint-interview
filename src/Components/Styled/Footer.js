import styled from 'styled-components';

export const FooterElement = styled.div`
	height: 0px;
	overflow: hidden;
	transition: height 400ms ease-out, opacity 1s linear;
	background: #fff;
	border: none;
	margin: 0;
	position: relative;
	bottom: 0;
	margin-bottom: 5px;

	> span.anticon {
		position: absolute;
		top: 10px;
		right: 10px;
		cursor: pointer;
		font-size: 18px;
		z-index: 9;
	}

	> .content {
		display: flex;
		align-items: center;
		flex-flow: row wrap;
		width: 100%;
		margin-bottom: 50px;

		.list {
			overflow: hidden;
			display: flex;
			align-items: flex-start;
			justify-content: space-between;
			width: 100%;
		}

		.list > div {
			display: flex;
			flex-flow: column nowrap;
			width: 50%;
			overflow: auto;

			&:last-child {
				border-left: solid 1px lightgray;
			}
		}

		.list div > div {
			display: flex;
			align-items: center;
			padding-left: 20px;

			span {
				width: 50%;
			}

			span:last-child {
				font-weight: 500;
			}
		}

		h5 {
			display: flex;
			align-items: center;
			margin: 0;
			margin-left: 10px;
			color: blue;
			padding: 10px;
		}
	}

	&.active {
		height: 450px;
		max-height: 40%;
		border: solid 1px lightgray;
		overflow: auto;
	}
`;
