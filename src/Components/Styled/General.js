import styled from 'styled-components';

export const ButtonElement = styled.div`
	button {
		display: flex;
		align-items: center;

		svg {
			margin: 0 5px;
		}

		p {
			margin: 0;
		}
	}
`;

export const SearchElement = styled.div``;
export const CheckboxElement = styled.div`
	label {
		display: flex;
		align-items: center;

		p {
			margin: 0;
		}
	}
`;
export const CheckboxGroupElement = styled.div``;
