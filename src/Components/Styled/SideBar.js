import styled from 'styled-components';

export const SideBarElement = styled.div`
	width: 200px;
	border-right: solid 1px lightgray;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;

	> .content {
	}

	ul {
		width: 100%;

		li {
			padding: 5px 10px;
			width: 100%;
			font-size: 15px;
			display: flex;
			align-items: center;
			height: 50px;
			font-weight: 500;
			cursor: pointer;
			transition: all ease-in-out 500ms;

			&:hover {
				background: gray;
				color: #fff;
			}

			&.active {
				background: lightgray;
			}
		}
	}
`;
