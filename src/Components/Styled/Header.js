import styled from 'styled-components';

export const HeaderElement = styled.div`
	width: 100%;
	height: 50px;
	border-bottom: solid 1px lightgray;
	overflow: hidden;
	display: flex;
	align-items: center;

	.anticon.loading {
		font-size: 20px;
		margin-left: 20px;
		color: #3a6eff;

		&.hidden {
			display: none;
		}
	}

	.tool-bar {
		display: flex;
		align-items: center;
		justify-content: center;
		width: 100%;

		.ant-btn {
			background: transparent;
			border: none;
			box-shadow: none;
			margin-right: 10px;
		}

		.filter {
			background: transparent;
			border: solid 1px lightgray;
		}

		.ant-input-search {
			max-width: 200px;
			margin-right: 10px;
		}
	}

	.counter {
		width: 100px;
		text-align: center;

		p {
			margin: 0;
		}
	}
`;
