import styled from 'styled-components';

export const FilterBarElement = styled.div`
	width: 0px;
	overflow: hidden;
	transition: width 400ms ease-out, opacity 1s linear;
	background: #fff;
	border: none;
	margin: 0;

	> .content {
		display: flex;
		flex-flow: column nowrap;

		.ant-radio-group {
			display: flex;
			flex-flow: column nowrap;
		}

		.ant-checkbox-group {
			display: flex;
			flex-flow: column nowrap;
		}

		h5 {
			height: 45px;
			border-bottom: solid 1px lightgray;
			display: flex;
			align-items: center;
			margin-left: 10px;
		}
	}

	&.active {
		width: 300px;
		max-width: 40%;
		margin: 5px 5px 25px 0;
		border: solid 1px lightgray;
	}
`;
