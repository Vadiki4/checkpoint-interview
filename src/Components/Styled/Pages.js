import styled from 'styled-components';

export const TablePageLayoutElement = styled.div`
	display: flex;
	width: 100%;

	.layout-body {
		overflow: hidden;
		margin-left: 200px;
		width: 90%;
	}

	.content {
		display: flex;
	}

	.table-content {
		display: flex;
		width: 100%;
		flex-flow: column nowrap;
		height: ${props => props.height - 50}px;
		padding: 0 10px;
	}
`;
