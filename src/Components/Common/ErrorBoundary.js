// Error boundary to catch unexpected errors
import React from 'react';

class ErrorBoundary extends React.Component {
	constructor(props) {
		super(props);
		this.state = { error: null, errorInfo: null };
	}

	static getDerivedStateFromError(error) {
		// Update state so the next render will show the fallback UI.
		console.error(error);
		return { hasError: true };
	}

	componentDidCatch(error, errorInfo) {
		// You can also log the error to an error reporting service
		this.setState({
			error: error,
			errorInfo: errorInfo,
		});
		console.error(error, errorInfo);
	}

	render() {
		if (this.state.hasError) {
			return (
				<div className='error-boundary'>
					<div className='wrapper'>
						<h1>Hmm..</h1>
						<p>The page is not found or Action Error found</p>
						<div className='buttons'>
							<a href='/'>Home Page</a>
							<br />
							<span>Sorry.</span>
						</div>
					</div>
					<div className='space'>
						<div className='blackhole' />
						<div className='ship' />
					</div>
				</div>
			);
		}
		return this.props.children;
	}
}

export default ErrorBoundary;
