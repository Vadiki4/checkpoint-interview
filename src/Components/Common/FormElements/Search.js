// Wrap component to customize library element

import React from 'react';
import { Input } from 'antd';
import { SearchElement } from '../../Styled/General';

// could be extended for more control with more props
const Search = React.memo(({ wrapClass, btnClass, ...props }) => {
	return (
		<SearchElement className={wrapClass}>
			<Input.Search className={btnClass} {...props} />
		</SearchElement>
	);
});

export default Search;
