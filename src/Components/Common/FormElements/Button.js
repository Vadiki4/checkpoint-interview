// Wrap component to customize library element

import React from 'react';
import { Button as ButtonLibraryElement } from 'antd';
import { ButtonElement } from '../../Styled/General';

const Button = React.memo(({ title, icon, wrapClass, btnClass, ...props }) => {
	return (
		<ButtonElement className={wrapClass}>
			<ButtonLibraryElement className={btnClass} {...props}>
				{icon}
				<p>{title}</p>
			</ButtonLibraryElement>
		</ButtonElement>
	);
});

export default Button;
