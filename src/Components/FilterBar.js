import React from 'react';
import { FilterBarElement } from '../Components/Styled/FilterBar';
import { Collapse, Checkbox, Radio } from 'antd';
const { Panel } = Collapse;

function FilterBar({ show, mailCategories, handleMailCategoryChange, selectedMailCategory, handleTimeChange, selectedMailTime }) {
	return (
		<FilterBarElement className={show ? 'active' : ''}>
			<div className='content'>
				<h5>FILTERS</h5>
				<Collapse defaultActiveKey={['time']}>
					<Panel header='Time' key='time'>
						<Radio.Group onChange={({ target }) => handleTimeChange(target.value)} value={selectedMailTime}>
							<Radio value={'today'}>Today</Radio>
							<Radio value={'last 7'}>Last 7 days</Radio>
							<Radio value={'last 30'}>Last 30 days</Radio>
						</Radio.Group>
					</Panel>
				</Collapse>
				<Collapse defaultActiveKey={['category']}>
					<Panel header='Category' key='category'>
						<Checkbox.Group options={mailCategories} value={selectedMailCategory} onChange={handleMailCategoryChange} />
					</Panel>
				</Collapse>
			</div>
		</FilterBarElement>
	);
}

export default FilterBar;
