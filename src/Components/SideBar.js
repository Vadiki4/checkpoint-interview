import React from 'react';
import { SideBarElement } from '../Components/Styled/SideBar';

function SideBar({ mailTypes = ['No Tabs'], selectedMailType, handleMailTypeSelect }) {
	return (
		<SideBarElement>
			<div className='content'>
				<ul>
					{mailTypes.map((tab, idx) => (
						<li key={idx} className={selectedMailType === tab ? 'active' : ''} onClick={() => handleMailTypeSelect(tab)}>
							{tab}
						</li>
					))}
				</ul>
			</div>
		</SideBarElement>
	);
}

export default SideBar;
