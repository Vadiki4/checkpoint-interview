import React from 'react';
import { FooterElement } from '../Components/Styled/Footer';
import { CloseOutlined } from '@ant-design/icons';
import { Tabs } from 'antd';
import { emailInfoFields, requestInfoFields } from '../Utilities/FooterHelper';
const { TabPane } = Tabs;

function Footer({ show, toggleFooter, selectedMailDetails, selectedMailType }) {
	return (
		<FooterElement className={show ? 'active' : ''}>
			<CloseOutlined onClick={toggleFooter} />
			<div className='content'>
				<Tabs defaultActiveKey='1'>
					<TabPane tab='GENERAL' key='1'>
						<div className='list'>
							{selectedMailType === 'Release requests' && (
								<div>
									<h5>Request Information</h5>
									{Object.keys(selectedMailDetails).map((item, idx) => {
										if (requestInfoFields.indexOf(item) >= 0)
											return (
												<div key={idx}>
													<span>{item}</span>
													<span>{selectedMailDetails[item]}</span>
												</div>
											);
									})}
								</div>
							)}
							<div>
								<h5>E-mail Information</h5>
								{Object.keys(selectedMailDetails).map((item, idx) => {
									if (emailInfoFields.indexOf(item) >= 0)
										return (
											<div key={idx}>
												<span>{item}</span>
												<span>{selectedMailDetails[item]}</span>
											</div>
										);
								})}
							</div>
						</div>
					</TabPane>
					<TabPane tab='SENDER INFO' key='2'>
						SENDER INFO
					</TabPane>
				</Tabs>
			</div>
		</FooterElement>
	);
}

export default Footer;
