import React, { useState, useEffect } from 'react';
import VirtualTable from '../Components/Common/VitualTable';
import { TableHolderElement } from '../Components/Styled/TableHolder';
import { wrongItems } from '../Utilities/TableHolderHelpers';

function TableHolder({ mailList, handleRowClick, height, handleRowSelect, selectedTableRows }) {
	const [columns, setColumns] = useState([]);
	const rowSelection = {
		selectedRowKeys: selectedTableRows,
		onChange: (selectedTableRows, selectedRows) => handleRowSelect(selectedTableRows, selectedRows),
	};

	useEffect(() => {
		function createColumnsFromList() {
			if (mailList && mailList.length) {
				const columns = [];
				for (const item in mailList[0]) {
					if (wrongItems.indexOf(item) < 0) {
						const newColumn = {};
						newColumn.title = item.replace(/(_)/g, ' ');
						newColumn.key = item.id;
						newColumn.dataIndex = item;
						columns.push(newColumn);
					}
				}
				setColumns(columns);
			} else {
				return setColumns([]);
			}
		}

		createColumnsFromList();
	}, [mailList]);

	function handleClick(record, { target }) {
		if (target.type === 'checkbox') {
			console.info('checkbox selected');
		} else {
			handleRowClick(record);
		}
	}

	return (
		<TableHolderElement height={height}>
			<div className='content'>
				<VirtualTable
					columns={columns}
					dataSource={mailList}
					onRow={record => ({ onClick: e => handleClick(record, e) })}
					rowSelection={rowSelection}
				/>
			</div>
		</TableHolderElement>
	);
}

export default TableHolder;
